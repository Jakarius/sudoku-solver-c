binaries=sudoku

buildall: sudoku

sudoku: sudoku.c
	clang sudoku.c -Wall -O3 -o sudoku 

.PHONY: clean

clean:
	rm -f $(binaries) *.o
