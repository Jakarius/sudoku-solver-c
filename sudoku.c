//CS2002 Practical 3: Killer Sudoku Solver
//140013168
//Tutor: Jan de Mujinck Hughes
//Date: 23/022016

#include <stdio.h>
#include <stdlib.h>
#include "sudoku.h"

int result = 4;
int sudoku[9][9];

int main(int argc, char const *argv[]) {

	if (argc == 3) {
		FILE* problem = fopen(argv[1], "r");
		FILE* solution = fopen(argv[2], "r");
		if (problem == 0 || solution == 0) {
			printf("Error opening file, exiting...\n");
			exit(0);
		}

		readArray(problem, solution);

		for (int i = 0; i < 9; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				printf("%d", sudoku[i][j]);
			}
			printf("\n");
		}

		switch (result) {
			case 0: printf("INVALIDPROBLEM\n");
					break;
			case 1: printf("INVALIDSOL\n");
					break;
			case 2: printf("INCOMPLETE\n");
					break;
			case 3: printf("SOLVED\n");
					break;
			default: printf("The program couldn't make any conclusion.\n");
		}

	} else {
		printf("Invalid arguments supplied, exiting.");
		exit(1);
	}
	return 0;
}

int readArray(FILE* problem, FILE* solution) {
	int val = 0;
	for (int i = 0; i < 9; i++) {
		int seenThisRow[9] = {0,0,0,0,0,0,0,0,0};
		int seenThisColumn[9] = {0,0,0,0,0,0,0,0,0};
		for (int j = 0; j < 9; j++) {
			if (fscanf(problem, "%d", &val) == 1) {

				//Check for out of bounds values.
				if (val > 9 || val < 1) {
					printf("Value greater than 9 or less than 1.\n");
					result = 0;
				}

				//Check for repeated values in a row.
				for (int k = 0; k < 9; k++) {
					if (val == seenThisRow[k]) {
						printf("Repeated value in row.\n");
						result = 0;
					}
				}

				//Checks for repeated values in a column, only once on second line.
				if (i > 0) {
					for (int k = 0; k < i; k++) {
						//Compare the most recently assigned value.
						if (val == sudoku[k][j]) {
							printf("Repeated value in column\n");
							result = 0;
						}
					}
				}

				seenThisColumn[i] = val;
				seenThisRow[j] = val;
				sudoku[i][j] = val;

			} else {
				printf("INVALIDPROBLEM\n");
				return 0;
			}
		}

	}
	return 0;
}